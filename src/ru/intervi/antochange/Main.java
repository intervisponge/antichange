package ru.intervi.antochange;

import com.google.inject.Inject;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.entity.DamageEntityEvent;
import org.spongepowered.api.event.entity.IgniteEntityEvent;
import org.spongepowered.api.event.entity.InteractEntityEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.world.ChangeWorldWeatherEvent;
import org.spongepowered.api.event.world.ExplosionEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Plugin(
        id = "antichange", name = "AntiChange", version = "1.0",
        description = "Block any changing (spread, physic, weather, etc)."
)
public class Main implements CommandExecutor {
    @Inject
    @DefaultConfig(sharedRoot = true)
    private Path defaultConfig;

    @Inject
    @DefaultConfig(sharedRoot = true)
    private ConfigurationLoader<CommentedConfigurationNode> configManager;

    private CommentedConfigurationNode config;

    private void setupConfig() {
        try {
            if (!Files.exists(defaultConfig)) {
                Files.createFile(defaultConfig);
                config = configManager.createEmptyNode();
                config.getNode("enabled").setValue(true);
                config.getNode("block", "break").setValue(true);
                config.getNode("block", "place").setValue(true);
                config.getNode("block", "decay").setValue(true);
                config.getNode("block", "modify").setValue(true);
                config.getNode("block", "pre").setValue(true);
                config.getNode("block", "interact").setValue(true);
                config.getNode("world", "explosion").setValue(true);
                config.getNode("world", "weather").setValue(true);
                config.getNode("entity", "damage").setValue(true);
                config.getNode("entity", "interact").setValue(true);
                config.getNode("entity", "ignite").setValue(true);
                config.getNode("entity", "pvp").setValue(true);
                configManager.save(config);
            } else {
                config = configManager.load();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Listener
    public void onPreInit(GamePreInitializationEvent event) {
        setupConfig();
        CommandSpec reload = CommandSpec.builder()
                .description(Text.of("Config reload command."))
                .permission("antichange.cmd.reload")
                .executor(this)
                .build();
        Sponge.getCommandManager().register(this, reload, "ac-reload", "antichange");
    }

    @Nonnull
    @Override
    public CommandResult execute(@Nonnull CommandSource src, @Nonnull CommandContext args) throws CommandException {
        setupConfig();
        src.sendMessage(Text.of("config reloaded"));
        return CommandResult.success();
    }

    @Listener
    public void onBreak(ChangeBlockEvent.Break event) {
        if (config.getNode("block", "break").getBoolean()) {
            event.setCancelled(true);
        }
    }

    @Listener
    public void onDecay(ChangeBlockEvent.Decay event) {
        if (config.getNode("block", "decay").getBoolean()) {
            event.setCancelled(true);
        }
    }

    @Listener
    public void onGrow(ChangeBlockEvent.Grow event) {
        if (config.getNode("block", "grow").getBoolean()) {
            event.setCancelled(true);
        }
    }

    @Listener
    public void onModify(ChangeBlockEvent.Modify event) {
        if (config.getNode("block", "modify").getBoolean()) {
            event.setCancelled(true);
        }
    }

    @Listener
    public void onPlace(ChangeBlockEvent.Place event) {
        if (config.getNode("block", "place").getBoolean()) {
            event.setCancelled(true);
        }
    }

    @Listener
    public void onPre(ChangeBlockEvent.Pre event) {
        if (config.getNode("block", "pre").getBoolean()) {
            event.setCancelled(true);
        }
    }

    @Listener
    public void onBlockInteract(InteractBlockEvent event) {
        if (config.getNode("block", "interact").getBoolean()) {
            event.setCancelled(true);
        }
    }

    @Listener
    public void onWeather(ChangeWorldWeatherEvent event) {
        if (config.getNode("world", "weather").getBoolean()) {
            event.setCancelled(true);
        }
    }

    @Listener
    public void onExplosion(ExplosionEvent.Pre event) {
        if (config.getNode("world", "explosion").getBoolean()) {
            event.setCancelled(true);
        }
    }

    @Listener
    public void onDamage(DamageEntityEvent event) {
        if (config.getNode("entity", "damage").getBoolean() && event.getCause().last(Player.class).isPresent()) {
            event.setCancelled(true);
        } else if (
                config.getNode("entity", "pvp").getBoolean()
                && event.getCause().first(Player.class).isPresent()
                && event.getCause().last(Player.class).isPresent()
        ) {
            event.setCancelled(true);
        }
    }

    @Listener
    public void onIgnite(IgniteEntityEvent event) {
        if (config.getNode("entity", "ignite").getBoolean()) {
            event.setCancelled(true);
        }
    }

    @Listener
    public void onEntityInteract(InteractEntityEvent event) {
        if (config.getNode("entity", "interact").getBoolean()) {
            event.setCancelled(true);
        }
    }
}
